import React, { useState } from 'react';
import axios from 'axios';

const fetchLargestId = async () => {
    try {
        const response = await axios.get('http://localhost:4000/api/v1');
        const data = response.data;

        let largestId = 0;
        data.forEach((item) => {
            if (item.id > largestId) {
                largestId = item.id;
            }
        });

        return largestId;
    } catch (error) {
        return null;
    }
};

const AddData = () => {
    const [newUsername, setNewUsername] = useState('');
    const [newEmail, setNewEmail] = useState('');

    const handleAddData = async () => {
        try {
            const largestId = await fetchLargestId();
            const newId = largestId + 1;
            const newData = {
                id: newId,
                username: newUsername,
                email: newEmail,
            };

            await axios.post('http://localhost:4000/api/v1', newData);

            window.location.replace("/");
        } catch (error) {
            alert(error);
        }
    };

    return (
        <div className="container">
            <h1 className="center">Add Data</h1>
            <div className="input">
                <div className="margin">
                    Username:
                    <input
                        type="text"
                        onChange={(e) => setNewUsername(e.target.value)}
                    />
                </div>
                <div className="margin">
                    Email:
                    <input
                        type="text"
                        onChange={(e) => setNewEmail(e.target.value)}
                    />
                </div>
            </div>
            <div className="margin center">
                <a className="edit" href="/" target="_blank" rel="noopener noreferrer">
                    <button >Back</button>
                </a>
                <button onClick={handleAddData}>Submit</button>
            </div>
        </div>
    );
};

export default AddData;
