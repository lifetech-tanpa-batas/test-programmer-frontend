import React, { useState } from 'react';
import { useParams } from "react-router-dom";
import axios from 'axios';

const Edit = () => {
  const [newUsername, setNewUsername] = useState('');
  const [newEmail, setNewEmail] = useState('');
  const { id } = useParams();

  const handleUpdate = async () => {
    try {
      const updatedData = {
        username: newUsername,
        email: newEmail,
      };

      await axios.put(`http://localhost:4000/api/v1/${id}`, updatedData);
      window.location.replace("/");
    } catch (error) {
      alert(error);
    }
  };

  return (
    <div className="container">
      <h1 className="center">Edit Data</h1>
      <div className="input">
        <div className="margin">
          Username:
          <input
            type="text"
            onChange={(e) => setNewUsername(e.target.value)}
          />
        </div>
        <div className="margin">
          Email:
          <input
            type="text"
            onChange={(e) => setNewEmail(e.target.value)}
          />
        </div>
      </div>
      <div className="margin center">
      <a className="edit" href="/" target="_blank" rel="noopener noreferrer">
        <button >Back</button>
      </a>
      <button onClick={handleUpdate}>Submit</button>
    </div>
    </div>
  );
};

export default Edit;
