import { useState, useEffect, useMemo } from "react";
import { useTable } from "react-table";
import axios from "axios";
import { saveAs } from "file-saver";
import * as XLSX from "xlsx";

const App = () => {
  const [data, setData] = useState([]);
  const [selectedRows, setSelectedRows] = useState(new Set());

  useEffect(() => {
    const getData = async () => {
      try {
        const response = await axios.get("http://localhost:4000/api/v1");
        const gotData = response.data;

        setData(gotData);
      } catch (error) {
        alert(error);
      }
    };
    getData();
  }, []);

  const columns = useMemo(() => {
    const handleDelete = async (row) => {
      try {
        const id = row.original.id;
  
        await axios.delete(`http://localhost:4000/api/v1/${id}`);
  
        // Muat ulang data setelah penghapusan
        const newData = data.filter((item) => !selectedRows.has(item.id));
        setData(newData);
  
        // Reset pilihan
        setSelectedRows(new Set());
      } catch (error) {
        console.error("Error deleting data:", error);
      }
    };
  
    const handleRowSelection = (row) => {
      const selectedRowIds = new Set(selectedRows);
  
      if (selectedRowIds.has(row.original.id)) {
        // Hapus dari daftar pilihan jika sudah ada
        selectedRowIds.delete(row.original.id);
      } else {
        // Tambahkan ke daftar pilihan jika belum ada
        selectedRowIds.add(row.original.id);
      }
  
      setSelectedRows(selectedRowIds);
    };
  
    return [
      {
        Header: " ",
        Cell: ({ row }) => (
          <input
            className="center"
            type="checkbox"
            checked={selectedRows.has(row.original.id)}
            onChange={() => handleRowSelection(row)}
          />
        ),
      },
      {
        Header: "ID",
        accessor: "id",
      },
      {
        Header: "Username",
        accessor: "username",
      },
      {
        Header: "Email",
        accessor: "email",
      },
      {
        Header: "Actions",
        Cell: ({ row }) => (
          <div>
            <a
              className="edit"
              href={`/edit/${row.original.id}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <button>Edit</button>
            </a>
            <button onClick={() => handleDelete(row)}>Delete</button>
          </div>
        ),
      },
    ];
  }, [selectedRows, data, setData, setSelectedRows]);

  const handleSelectAll = () => {
    if (selectedRows.size === data.length) {
      // Deselect All: Clear the selectedRows set
      setSelectedRows(new Set());
    } else {
      // Select All: Add all row IDs to selectedRows set
      const allRowIds = data.map((item) => item.id);
      setSelectedRows(new Set(allRowIds));
    }
  };
  
  const table = useTable({ columns, data });
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = table;

  const handleDeleteSelected = async () => {
    try {
      // Hapus semua baris yang dipilih
      for (const id of selectedRows) {
        await axios.delete(`http://localhost:4000/api/v1/${id}`);
      }
  
      // Muat ulang data setelah penghapusan
      const newData = data.filter((item) => !selectedRows.has(item.id));
      setData(newData);
  
      // Reset pilihan
      setSelectedRows(new Set());
    } catch (error) {
      console.error("Error deleting data:", error);
    }
  };

  const exportToExcel = () => {
    try {
      const worksheet = XLSX.utils.json_to_sheet(data);
      const workbook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(workbook, worksheet, "Data");
      const excelBuffer = XLSX.write(workbook, {
        bookType: "xlsx",
        type: "array",
      });

      const blob = new Blob([excelBuffer], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });
      saveAs(blob, "table.xlsx");
    } catch (error) {
      alert(error);
    }
  };

  return (
    <div className="container">
      <div className="margin">
        <input type="checkbox" onChange={handleSelectAll} />
        <button onClick={handleDeleteSelected} disabled={selectedRows.size === 0}>Delete selected</button>
        <a href="/add" target="_blank" rel="noopener noreferrer">
          <button className="margin">Add Data</button>
        </a>
        <button onClick={exportToExcel}>Export to Excel</button>
      </div>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>
                  {column.render("Header")}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {rows.map((row) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default App;
